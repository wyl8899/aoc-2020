#[allow(unused_imports)]
use aoc_2020::EasilyParsable;
use std::{collections::HashMap, collections::HashSet, error::Error, fs};

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let input = fs::read_to_string("input07")?;
    let result = solve(&input)?;
    println!("{}", result);
    Ok(())
}

fn solve(input: &str) -> Result<usize, Box<dyn Error + Send + Sync>> {
    let input = parse(input);
    // let result = find_bag_01("shiny gold", &mut HashSet::new(), &input);
    let result = find_bag_02("shiny gold", &input);
    Ok(result)
}

fn parse(input: &str) -> HashMap<String, HashMap<String, usize>> {
    input
        .split('\n')
        .filter(|s| !s.is_empty())
        .filter_map(|s| s.split_once_s("bags contain"))
        .map(|(bag, contents)| {
            let contents: HashMap<String, usize> = contents
                .split(',')
                .filter_map(|bag| {
                    let bag = bag.trim().split_whitespace().collect::<Vec<_>>();
                    bag.get(0)
                        .map(|n| n.parse::<usize>().ok())
                        .flatten()
                        .map(|n| {
                            let type_ = bag.get(1).unwrap().to_string() + " " + bag.get(2).unwrap();
                            (type_, n)
                        })
                })
                .collect();

            (bag.to_string(), contents)
        })
        .collect()
}

#[allow(dead_code)]
fn find_bag_01(
    bag: &str,
    visited: &mut HashSet<String>,
    input: &HashMap<String, HashMap<String, usize>>,
) -> usize {
    let mut result = 0;
    for (inner_bag, contents) in input {
        if visited.contains(inner_bag) {
            continue;
        }
        if contents.contains_key(bag) {
            visited.insert(inner_bag.to_string());
            result += 1 + find_bag_01(inner_bag, visited, input);
        }
    }
    result
}

fn find_bag_02(bag: &str, input: &HashMap<String, HashMap<String, usize>>) -> usize {
    let mut result = 0;
    for (inner_bag, n) in input.get(bag).unwrap() {
        result += n + n * find_bag_02(inner_bag, input);
    }
    result
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test() {
        let input = r#"light red bags contain 1 bright white bag, 2 muted yellow bags.
        dark orange bags contain 3 bright white bags, 4 muted yellow bags.
        bright white bags contain 1 shiny gold bag.
        muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
        shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
        dark olive bags contain 3 faded blue bags, 4 dotted black bags.
        vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
        faded blue bags contain no other bags.
        dotted black bags contain no other bags."#;
        let input = parse(input);
        let result = find_bag_01("shiny gold", &mut HashSet::new(), &input);
        assert_eq!(result, 4);

        let result = find_bag_02("shiny gold", &input);
        assert_eq!(result, 32);
    }
}
