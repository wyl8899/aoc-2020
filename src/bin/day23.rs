#[allow(unused_imports)]
use aoc_2020::EasilyParsable;

fn main() {
    let input = "624397158";
    let result = solve(&input, 10_000_000);
    println!("result = {}", result);
}

fn solve(input: &str, moves: usize) -> usize {
    let input: Vec<usize> = input
        .chars()
        .filter_map(|s| s.to_digit(10))
        .map(|d| d as usize)
        .collect();

    let mut cups = init(&input, 1_000_000);

    for _ in 0..moves {
        make_move(&mut cups);
    }

    let n1 = cups[1];
    let n2 = cups[n1];

    n1 * n2
}

fn init(input: &[usize], len: usize) -> Vec<usize> {
    let mut result = Vec::with_capacity(len + 1);
    for cup in 0..len + 1 {
        result.push(cup + 1)
    }

    for i in 0..input.len() - 1 {
        let value = input[i];
        let points_to = input[i + 1];

        result[value] = points_to;
    }

    result[0] = input[0];

    result[input[input.len() - 1]] = if input.len() == len {
        input[0]
    } else {
        input.len() + 1
    };

    if result.len() > input.len() + 1 {
        let cup = result.len() - 1;
        result[cup] = input[0];
    }

    result
}

fn make_move(cups: &mut [usize]) {
    let cur = cups[0];

    let n1 = cups[cur];
    let n2 = cups[n1];
    let n3 = cups[n2];
    cups[cur] = cups[n3];
    cups[0] = cups[n3];

    let mut dest = cur - 1;
    while dest == n1 || dest == n2 || dest == n3 || dest == 0 {
        if dest == 0 {
            dest = cups.len() - 1;
        } else {
            dest -= 1;
        }
    }

    let cut = cups[dest];
    cups[dest] = n1;
    cups[n3] = cut;
}

#[cfg(test)]
mod tests {
    use super::*;

    fn cups_to_vec(input: &[usize]) -> Vec<usize> {
        let mut cur = input[0];
        let mut result = Vec::with_capacity(input.len() - 1);
        for _ in 0..input.len() - 1 {
            result.push(cur);
            cur = input[cur];
        }
        result
    }

    #[test]
    fn test_cups_to_vec() {
        let cups = vec![3, 8, 9, 1, 2, 5, 4, 6, 7];
        let result = init(&cups, cups.len());
        let new_cups = cups_to_vec(&result);
        assert_eq!(cups, new_cups);
    }

    #[test]
    fn test_init() {
        let cups = vec![3, 8, 9, 1, 2, 5, 4, 6, 7];
        let result = init(&cups, cups.len());
        println!("cups=\t   {:?}", cups);
        println!("result=\t{:?}", result);
        assert_eq!(result, vec![3, 2, 5, 8, 6, 4, 7, 3, 9, 1]);
    }

    #[test]
    fn test_make_move() {
        let input = vec![3, 8, 9, 1, 2, 5, 4, 6, 7];
        let mut cups = init(&input, input.len());
        make_move(&mut cups);
        assert_eq!(cups_to_vec(&cups), vec![2, 8, 9, 1, 5, 4, 6, 7, 3,]);
        make_move(&mut cups);
        assert_eq!(cups_to_vec(&cups), vec![5, 4, 6, 7, 8, 9, 1, 3, 2,]);
        make_move(&mut cups);
        assert_eq!(cups_to_vec(&cups), vec![8, 9, 1, 3, 4, 6, 7, 2, 5,]);
        make_move(&mut cups);
        assert_eq!(cups_to_vec(&cups), vec![4, 6, 7, 9, 1, 3, 2, 5, 8,]);
        make_move(&mut cups);
        assert_eq!(cups_to_vec(&cups), vec![1, 3, 6, 7, 9, 2, 5, 8, 4,]);
        make_move(&mut cups);
        assert_eq!(cups_to_vec(&cups), vec![9, 3, 6, 7, 2, 5, 8, 4, 1,]);
        make_move(&mut cups);
        assert_eq!(cups_to_vec(&cups), vec![2, 5, 8, 3, 6, 7, 4, 1, 9,]);
        make_move(&mut cups);
        assert_eq!(cups_to_vec(&cups), vec![6, 7, 4, 1, 5, 8, 3, 9, 2,]);
        make_move(&mut cups);
        assert_eq!(cups_to_vec(&cups), vec![5, 7, 4, 1, 8, 3, 9, 2, 6]);
        make_move(&mut cups);
        assert_eq!(cups_to_vec(&cups), vec![8, 3, 7, 4, 1, 9, 2, 6, 5,]);
    }

    #[test]
    fn test() {
        let input = r"389125467";
        let result = solve(input, 10_000_000);
        assert_eq!(result, 149245887792);
    }
}
