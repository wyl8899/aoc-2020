#[allow(unused_imports)]
use aoc_2020::EasilyParsable;
use num_integer::Integer;
use std::{error::Error, fs};

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let input = fs::read_to_string("input13")?;
    let result = solve(&input)?;
    println!("{}", result);
    Ok(())
}

fn solve(input: &str) -> Result<u64, Box<dyn Error + Send + Sync>> {
    let mut input = input
        .split('\n')
        .map(|s| s.trim())
        .filter(|s| !s.is_empty());

    let _depart = input.next().unwrap().parse::<u64>()?;
    let mut tt: Vec<_> = input
        .next()
        .unwrap()
        .split(',')
        .enumerate()
        .filter_map(|(i, s)| s.parse::<u64>().map(|s| (i as u64, s)).ok())
        .collect();

    tt.sort_by(|(_, t), (_, ot)| ot.cmp(t));

    let mut iter = tt.iter();

    let (mut t, mut step) = iter.next().unwrap();
    t = step - t;

    for (offset, bus) in iter {
        println!("t={}, offset={}, bus={}", t, offset, bus);
        loop {
            if (t + offset) % bus == 0 {
                step = step.lcm(bus);
                break;
            }
            t += step;
        }
    }

    Ok(t)
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test() {
        let input = r"939
        7,13,x,x,59,x,31,19";
        let result = solve(input);
        assert!(result.is_ok());

        let result = result.unwrap();
        assert_eq!(result, 1068781);
    }
}
