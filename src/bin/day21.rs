#[allow(unused_imports)]
use aoc_2020::EasilyParsable;
use std::{
    collections::{HashMap, HashSet},
    error::Error,
    fs,
};

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let input = fs::read_to_string("input21")?;
    let result = solve(&input)?;
    println!("{}", result);
    Ok(())
}

fn solve(input: &str) -> Result<usize, Box<dyn Error + Send + Sync>> {
    let input: Vec<_> = input
        .split('\n')
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .map(|s| {
            let (ingredients, allergens) = s.split_once_s(" (contains ").unwrap();
            let ingredient = ingredients.split(' ').map(str::trim).collect::<Vec<_>>();
            let allergens = &allergens[..allergens.len() - 1];
            let allergens = allergens.split(", ").map(str::trim).collect::<Vec<_>>();
            (ingredient, allergens)
        })
        .collect();

    let mut suspects = HashMap::new();
    for (ingredients, allergens) in &input {
        for allergen in allergens {
            let suspects = suspects.entry(allergen).or_insert_with(HashSet::new);
            if suspects.is_empty() {
                for &ingredient in ingredients {
                    suspects.insert(ingredient);
                }
            } else {
                let safe = suspects
                    .iter()
                    .filter(|suspect| !ingredients.contains(suspect))
                    .copied()
                    .collect::<Vec<_>>();
                for s in safe {
                    suspects.remove(s);
                }
            }
        }
    }

    let mut dangerous = HashMap::new();
    for (allergen, ingredients) in suspects {
        for ingredient in ingredients {
            let allergens = dangerous.entry(ingredient).or_insert_with(HashSet::new);
            allergens.insert(allergen);
        }
    }

    let mut result = 0;
    for (ingredients, _) in &input {
        for ingredient in ingredients {
            if !dangerous.contains_key(ingredient) {
                result += 1;
            }
        }
    }

    // ingredient => allergen
    let mut known = HashMap::new();
    let target_len = dangerous.len();

    while known.len() != target_len {
        for (&ingredient, allergens) in &mut dangerous {
            if allergens.is_empty() {
                panic!("Something is wrong!");
            }
            if allergens.len() == 1 {
                if !known.contains_key(ingredient) {
                    known.insert(ingredient, allergens.iter().next().copied().unwrap());
                }
            } else {
                for allergen in known.values() {
                    allergens.remove(allergen);
                }
            }
        }
    }

    let mut known = known.iter().collect::<Vec<_>>();
    known.sort_by_key(|(_, &&allergen)| allergen);
    let answer = known
        .iter()
        .map(|(&ingredient, _)| ingredient)
        .collect::<Vec<_>>()
        .join(",");
    println!("{}", answer);
    Ok(result)
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test() {
        let input = r"mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
        trh fvjkl sbzzf mxmxvkd (contains dairy)
        sqjhc fvjkl (contains soy)
        sqjhc mxmxvkd sbzzf (contains fish)";
        let result = solve(input);
        assert!(result.is_ok());

        let result = result.unwrap();
        assert_eq!(result, 5);
    }
}
