use std::{error::Error, fs};

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let input = fs::read_to_string("input03")?;
    let input = input
        .split('\n')
        .filter(|s| !s.is_empty())
        .map(|s| s.chars().collect::<Vec<_>>())
        .collect::<Vec<_>>();
    let r0 = count_trees((0, 0), (1, 1), &input);
    let r1 = count_trees((0, 0), (1, 3), &input);
    let r2 = count_trees((0, 0), (1, 5), &input);
    let r3 = count_trees((0, 0), (1, 7), &input);
    let r4 = count_trees((0, 0), (2, 1), &input);
    let result = r0 * r1 * r2 * r3 * r4;
    println!("{}", result);
    Ok(())
}

fn count_trees((r, c): (usize, usize), (rs, cs): (usize, usize), chars: &[Vec<char>]) -> u64 {
    if r >= chars.len() {
        return 0;
    }
    let x = if c < chars[0].len() {
        c
    } else {
        c - chars[0].len()
    };

    let result = if chars[r][x] == '#' { 1 } else { 0 };

    result + count_trees((r + rs, x + cs), (rs, cs), chars)
}
