#[macro_use]
extern crate lazy_static;

use std::{collections::HashMap, str::FromStr};
use std::{error::Error, fs};

use regex::Regex;
use validator::{Validate, ValidationError};

use aoc_2020::EasilyParsable;

lazy_static! {
    static ref HCL_REGEX: Regex = Regex::new(r"#[0-9a-f]{6}").unwrap();
    static ref ECL_REGEX: Regex = Regex::new(r"(amb|blu|brn|gry|grn|hzl|oth)").unwrap();
}

#[derive(Debug, Validate)]
struct Passport {
    #[validate(range(min = 1920, max = 2002))]
    byr: u16,
    #[validate(range(min = 2010, max = 2020))]
    iyr: u16,
    #[validate(range(min = 2020, max = 2030))]
    eyr: u16,
    #[validate(custom = "validate_hgt")]
    hgt: String,
    #[validate(regex = "HCL_REGEX")]
    hcl: String,
    #[validate(regex = "ECL_REGEX")]
    ecl: String,
    #[validate(length(equal = 9), custom = "validate_pid")]
    pid: String,
    cid: Option<String>,
}

fn validate_hgt(hgt: &str) -> Result<(), ValidationError> {
    let err: ValidationError = ValidationError::new("hgt validate error");
    let (value, cm) = hgt.split_at(hgt.len() - 2);
    let value = value.parse::<u16>().map_err(|_| err)?;
    let err = Err(ValidationError::new("Bad number"));
    match cm {
        "cm" => {
            if 150 <= value && value <= 193 {
                Ok(())
            } else {
                err
            }
        }
        "in" => {
            if value >= 59 && value <= 76 {
                Ok(())
            } else {
                err
            }
        }
        _ => err,
    }
}

fn validate_pid(pid: &str) -> Result<(), ValidationError> {
    let err: ValidationError = ValidationError::new("pid validate error");
    pid.parse::<u32>().map_err(|_| err)?;
    Ok(())
}

impl FromStr for Passport {
    type Err = ValidationError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let err: ValidationError = ValidationError::new("passport parse error");
        let fields: HashMap<_, _> = s
            .split_whitespace()
            .filter_map(|s| s.split_once_(':'))
            .collect();

        let get_field = |s| {
            fields
                .get(s)
                .map(|s| s.to_string())
                .ok_or_else(|| ValidationError::new(s))
        };

        let get_u16_field =
            |s| get_field(s).and_then(|s| s.parse::<u16>().map_err(|_| err.clone()));

        Ok(Passport {
            byr: get_u16_field("byr")?,
            iyr: get_u16_field("iyr")?,
            eyr: get_u16_field("eyr")?,
            hgt: get_field("hgt")?,
            hcl: get_field("hcl")?,
            ecl: get_field("ecl")?,
            pid: get_field("pid")?,
            cid: fields.get("cid").map(|s| s.to_string()),
        })
    }
}

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let input = fs::read_to_string("input04")?;
    let solution = solve(&input)?;
    println!("{}", solution);
    Ok(())
}

fn solve(input: &str) -> Result<usize, Box<dyn Error + Send + Sync>> {
    let result: Vec<_> = input
        .split("\n\n")
        .filter_map(|s| Passport::from_str(s).ok())
        .filter_map(|p| {
            let result = p.validate();
            // println!("{:#?}", result);
            result.ok()
        })
        .collect();
    Ok(result.len())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parse_passport() {
        let passport = Passport::from_str(
            r#"ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm"#,
        );
        assert!(passport.is_ok());
        let passport = passport.unwrap();
        assert!(passport.byr == 1937);

        let passport = Passport::from_str(
            r#"iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929"#,
        );
        assert!(passport.is_err());

        let passport = Passport::from_str(
            r#"hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm"#,
        );
        assert!(passport.is_ok());
        let passport = passport.unwrap();
        assert!(passport.cid.is_none());

        let passport = Passport::from_str(
            r#"hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in"#,
        );
        assert!(passport.is_err());
    }

    #[test]
    fn test_invalid() {
        let data = r#"eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007"#;

        let result = solve(data);
        assert!(result.is_ok());
        let result = result.unwrap();
        assert_eq!(result, 0);
    }

    #[test]
    fn test_valid() {
        let data = r#"pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719"#;

        let result = solve(data);
        assert!(result.is_ok());
        let result = result.unwrap();
        assert_eq!(result, 4);
    }
}
