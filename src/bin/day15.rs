#[allow(unused_imports)]
use aoc_2020::EasilyParsable;
use std::{collections::HashMap, error::Error, fs};

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let input = fs::read_to_string("input15")?;
    let result = solve(&input, 30000000)?;
    println!("{}", result);
    Ok(())
}

fn solve(input: &str, turns: usize) -> Result<usize, Box<dyn Error + Send + Sync>> {
    let input: Vec<_> = input
        .split('\n')
        .next()
        .unwrap()
        .split(',')
        .filter_map(|s| s.parse::<usize>().ok())
        .collect();
    let mut mem = HashMap::new();
    let mut prev = 0;
    let mut last = 0;
    for i in 1..=turns {
        if i <= input.len() {
            last = input[i - 1];
        } else {
            last = say(&mem, last, i);
        }

        // println!(
        //     "prev={} was said at {:?}, now {}, so saying {}",
        //     prev,
        //     mem.get(&prev),
        //     i,
        //     last
        // );

        mem.insert(prev, i - 1);

        prev = last;
    }

    Ok(last)
}

fn say(mem: &HashMap<usize, usize>, last: usize, turn: usize) -> usize {
    if let Some(prev) = mem.get(&last) {
        turn - prev - 1
    } else {
        0
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test() {
        let input = r"0,3,6";
        let result = solve(input, 2020);
        assert!(result.is_ok());

        let result = result.unwrap();
        assert_eq!(result, 436);
    }
}
