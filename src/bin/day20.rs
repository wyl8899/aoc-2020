#[allow(unused_imports)]
use aoc_2020::EasilyParsable;
use bitvec::prelude::*;
use std::{
    cmp::min,
    collections::{HashMap, HashSet},
    error::Error,
    fmt::Debug,
    fs,
    str::FromStr,
};

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
enum Dir {
    Up,
    Right,
    Down,
    Left,
}

impl Dir {
    fn all() -> [Self; 4] {
        [Dir::Up, Dir::Right, Dir::Down, Dir::Left]
    }

    fn to_index(&self) -> usize {
        match self {
            Dir::Up => 0,
            Dir::Right => 1,
            Dir::Down => 2,
            Dir::Left => 3,
        }
    }

    fn from_index(n: usize) -> Self {
        match n {
            0 => Dir::Up,
            1 => Dir::Right,
            2 => Dir::Down,
            3 => Dir::Left,
            _ => unreachable!(),
        }
    }

    fn opposite(&self) -> Self {
        match self {
            Dir::Up => Dir::Down,
            Dir::Right => Dir::Left,
            Dir::Down => Dir::Up,
            Dir::Left => Dir::Right,
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct Orientation {
    rotation: Dir,
    flip: bool,
}

impl Debug for Orientation {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?} {}", self.rotation, self.flip)
    }
}

impl Orientation {
    #[allow(dead_code)]
    fn new(rotation: Dir, flip: bool) -> Self {
        Self { rotation, flip }
    }
    fn all() -> [Self; 16] {
        let mut result = [Self::default(); 16];
        let mut i = 0;
        for &rotation in &Dir::all() {
            for &flip in &[false, true] {
                let orientation = Self { rotation, flip };
                result[i] = orientation;
                i += 1;
            }
        }
        result
    }
}

impl Default for Orientation {
    fn default() -> Self {
        Self {
            rotation: Dir::Up,
            flip: false,
        }
    }
}

#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct Tile {
    id: u64,
    data: Vec<BitVec>,
    size: usize,
}

impl Tile {
    fn get_borders(&self, orientation: &Orientation) -> [BitVec; 4] {
        let mut b1 = BitVec::<LocalBits, usize>::new();
        let mut b2 = BitVec::<LocalBits, usize>::new();
        let mut b3 = BitVec::<LocalBits, usize>::new();
        let mut b4 = BitVec::<LocalBits, usize>::new();
        for i in 0..self.size {
            b1.push(self.data[0][i]);
            b2.push(self.data[i][self.size - 1]);
            b3.push(self.data[self.size - 1][self.size - i - 1]);
            b4.push(self.data[self.size - i - 1][0]);
        }

        if orientation.flip {
            std::mem::swap(&mut b2, &mut b4);
            b1 = b1.into_iter().rev().collect();
            b3 = b3.into_iter().rev().collect();
        }
        match orientation.rotation {
            Dir::Up => [b1, b2, b3, b4],
            Dir::Right => [b2, b3, b4, b1],
            Dir::Down => [b3, b4, b1, b2],
            Dir::Left => [b4, b1, b2, b3],
        }
    }

    fn get_all_possible_borders(&self) -> HashMap<[BitVec; 4], Orientation> {
        let mut result = HashMap::new();
        for orientation in &Orientation::all() {
            result.insert(self.get_borders(orientation), *orientation);
        }
        result
    }

    fn trim(&mut self) {
        assert!(self.size > 2);
        self.size -= 2;

        let new_data = self
            .data
            .drain(1..self.data.len() - 1)
            .map(|mut v| v.drain(1..v.len() - 1).collect::<BitVec>())
            .collect::<Vec<_>>();

        self.data = new_data;
    }

    fn rotate(&mut self, orientation: &Orientation) {
        if orientation.flip {
            for row in self.data.iter_mut() {
                row.reverse();
            }
        }

        match orientation.rotation {
            Dir::Up => {}
            Dir::Right => {
                let mut new_data = Vec::with_capacity(self.size);
                for i in 0..self.size {
                    let mut v = BitVec::with_capacity(self.size);
                    for j in 0..self.size {
                        v.push(self.data[j][self.size - i - 1]);
                    }
                    new_data.push(v);
                }
                self.data = new_data;
            }
            Dir::Down => {
                self.data.reverse();
                for row in self.data.iter_mut() {
                    row.reverse();
                }
            }
            Dir::Left => {
                let mut new_data = Vec::with_capacity(self.size);
                for i in 0..self.size {
                    let mut v = BitVec::with_capacity(self.size);
                    for j in 0..self.size {
                        v.push(self.data[self.size - j - 1][i]);
                    }
                    new_data.push(v);
                }
                self.data = new_data;
            }
        }
    }

    fn count_monsters(&self) -> usize {
        let mut result = 0;
        let monster = get_monster();
        for (i, r) in self.data.iter().enumerate() {
            for (j, _) in r.iter().enumerate() {
                let mut is_monster = true;
                for (mi, mj) in monster.iter() {
                    if i + mi >= self.size || j + mj >= self.size {
                        is_monster = false;
                        break;
                    }
                    let dot = self.data[i + mi][j + mj];
                    if !dot {
                        is_monster = false;
                        break;
                    }
                }
                if is_monster {
                    result += 1;
                }
            }
        }
        result
    }

    fn count_all_monster(&self) -> usize {
        Orientation::all()
            .iter()
            .map(|orientation| {
                let mut puzzle = self.clone();
                puzzle.rotate(orientation);
                puzzle.count_monsters()
            })
            .max()
            .unwrap()
    }

    fn count_all_ones(&self) -> usize {
        self.data.iter().map(|v| v.count_ones()).sum()
    }
}

impl Debug for Tile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Tile {}:", self.id)?;
        for row in &self.data {
            writeln!(f)?;
            write!(f, "{}", row)?;
        }
        Ok(())
    }
}

fn get_monster() -> Vec<(usize, usize)> {
    let mut result = Vec::new();
    let monster = include_str!("day20_monster");

    for (i, row) in monster.split('\n').enumerate() {
        for (j, c) in row.chars().enumerate() {
            if c == '#' {
                result.push((i, j));
            }
        }
    }

    result
}

impl FromStr for Tile {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut s_iter = s.split('\n').map(|s| s.trim()).filter(|s| !s.is_empty());
        let id = if s.starts_with("Tile") {
            s_iter
                .next()
                .map(|s| s[5..9].parse::<u64>())
                .unwrap()
                .unwrap()
        } else {
            0
        };

        let data: Vec<BitVec> = s_iter
            .map(|r| {
                let mut bv = BitVec::<LocalBits, usize>::new();
                r.chars().for_each(|c| bv.push(char_to_bool(&c)));
                bv
            })
            .collect();

        let size = data.iter().map(|v| v.len()).max().unwrap();
        assert_eq!(size, data.len());

        Ok(Self { id, data, size })
    }
}

fn char_to_bool(c: &char) -> bool {
    match c {
        '.' => false,
        '#' => true,
        _ => panic!("WTF?"),
    }
}

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let input = fs::read_to_string("input20")?;
    solve_part_2(&input);
    Ok(())
}

#[allow(dead_code)]
fn solve_part_1(input: &str) -> u64 {
    let field = solve_puzzle(input);
    let l = field.inner.len() - 1;

    let e1 = field.inner[0][0].as_ref().unwrap().0.id;
    let e2 = field.inner[0][l].as_ref().unwrap().0.id;
    let e3 = field.inner[l][0].as_ref().unwrap().0.id;
    let e4 = field.inner[l][l].as_ref().unwrap().0.id;

    e1 * e2 * e3 * e4
}

fn solve_part_2(input: &str) -> usize {
    let field = solve_puzzle(input);

    let puzzle = field.to_tile(0);

    let n_monsters = puzzle.count_all_monster();
    println!("monsters_found={}", n_monsters);

    let result = puzzle.count_all_ones() - n_monsters * get_monster().len();

    println!("result={}", result);

    result
}

fn parse(input: &str) -> HashMap<u64, Tile> {
    input
        .split("\n\n")
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .filter_map(|s| Tile::from_str(s).ok())
        .map(|tile| (tile.id, tile))
        .collect()
}

fn solve_puzzle(input: &str) -> Field {
    let tiles: HashMap<u64, Tile> = parse(input);

    let mut all_tile_borders: [HashMap<BitVec, HashSet<(u64, Orientation)>>; 4] = [
        HashMap::new(),
        HashMap::new(),
        HashMap::new(),
        HashMap::new(),
    ];

    for (tile_id, tile) in &tiles {
        for (borders, orientation) in tile.get_all_possible_borders() {
            for (n, border) in borders.iter().enumerate() {
                let current_border_map = &mut all_tile_borders[n];
                let entry = current_border_map
                    .entry(normalize_border(border.clone()))
                    .or_insert_with(HashSet::new);
                entry.insert((*tile_id, orientation));
            }
        }
    }

    let field_size = (tiles.len() as f64).sqrt();
    assert!(field_size.fract() == 0.0);
    let field_size = field_size as usize;

    let mut field = Field::new(field_size, all_tile_borders, tiles);
    field.solve();

    field
}

fn normalize_border(border: BitVec) -> BitVec {
    let mut b1 = border.clone();
    b1.reverse();
    min(border, b1)
}

#[derive(Clone, PartialEq, Eq)]
enum Border {
    Any,
    None,
    Fixed(BitVec),
}

impl Debug for Border {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Border::Any => {
                write!(f, "Any")
            }
            Border::None => {
                write!(f, "None")
            }
            Border::Fixed(v) => {
                write!(f, "Fixed{}", v)
            }
        }
    }
}

struct Field {
    inner: Vec<Vec<Option<(Tile, Orientation)>>>,
    used: HashSet<u64>,
    size: usize,
    borders: [HashMap<BitVec, HashSet<(u64, Orientation)>>; 4],
    tiles: HashMap<u64, Tile>,
}

impl Field {
    fn new(
        size: usize,
        borders: [HashMap<BitVec, HashSet<(u64, Orientation)>>; 4],
        tiles: HashMap<u64, Tile>,
    ) -> Self {
        Field {
            inner: vec![vec![Option::<(Tile, Orientation)>::None; size]; size],
            used: HashSet::with_capacity(tiles.len()),
            size,
            borders,
            tiles,
        }
    }

    fn solve(&mut self) {
        for i in 0..self.size {
            for j in 0..self.size {
                let mut n_borders: [Border; 4] =
                    [Border::None, Border::None, Border::None, Border::None];
                for (n, n_border) in n_borders.iter_mut().enumerate() {
                    *n_border = self.get_border(i, j, n);
                }
                let tile = self.find_suitable_tile(&n_borders);
                self.used.insert(tile.0.id);
                self.inner[i][j] = Some(tile);
            }
        }
    }

    fn get_border(&self, i: usize, j: usize, n: usize) -> Border {
        let dir = Dir::from_index(n);

        if dir == Dir::Up && i == 0 {
            return Border::None;
        }
        if dir == Dir::Right && j == self.size - 1 {
            return Border::None;
        }
        if dir == Dir::Down && i == self.size - 1 {
            return Border::None;
        }
        if dir == Dir::Left && j == 0 {
            return Border::None;
        }

        let (ni, nj) = match dir {
            Dir::Up => (i - 1, j),
            Dir::Right => (i, j + 1),
            Dir::Down => (i + 1, j),
            Dir::Left => (i, j - 1),
        };

        let nn = dir.opposite().to_index();

        match &self.inner[ni][nj] {
            Some((tile, orientation)) => {
                let border = tile.get_borders(orientation)[nn].clone();
                Border::Fixed(border)
            }
            None => Border::Any,
        }
    }

    fn find_suitable_tile(&self, n_borders: &[Border; 4]) -> (Tile, Orientation) {
        let mut suspects: HashSet<(u64, Orientation)> = HashSet::new();

        for (n, n_border) in n_borders.iter().enumerate() {
            match n_border {
                Border::Any => {}
                Border::None => {
                    let fit_ids = self.borders[n]
                        .values()
                        .filter(|tiles| {
                            let dedup: HashSet<_> = tiles.iter().map(|(id, _)| id).collect();
                            dedup.len() == 1
                        })
                        .map(|tiles| tiles.iter().map(|(id, orientation)| (id, orientation)))
                        .flatten()
                        .filter(|(id, _)| !self.used.contains(id))
                        .map(|(id, orientation)| (*id, *orientation))
                        .collect();
                    if suspects.is_empty() {
                        suspects = fit_ids
                    } else {
                        suspects = suspects.intersection(&fit_ids).cloned().collect();
                    }
                }
                Border::Fixed(border) => {
                    let borders = &self.borders[n];
                    let with_same_border = borders.get(&normalize_border(border.clone())).unwrap();
                    let fit_ids = with_same_border
                        .iter()
                        .filter(|(id, _)| !self.used.contains(id))
                        .map(|(id, orientation)| (*id, *orientation))
                        .collect();
                    if suspects.is_empty() {
                        suspects = fit_ids
                    } else {
                        suspects = suspects.intersection(&fit_ids).cloned().collect();
                    }
                }
            };
        }
        assert!(!suspects.is_empty());
        let (result_id, orientation) = suspects.iter().next().unwrap();
        let tile = self.tiles.get(result_id).unwrap();
        (tile.clone(), *orientation)
    }

    fn to_tile(&self, id: u64) -> Tile {
        let mut field = self
            .inner
            .iter()
            .map(|v| {
                v.clone()
                    .into_iter()
                    .map(Option::unwrap)
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();

        for row in field.iter_mut() {
            for (tile, orientation) in row {
                tile.trim();
                tile.rotate(orientation);
            }
        }

        let mut picture = Vec::with_capacity(field.len());
        for row in field.iter() {
            for i in 0..row[0].0.size {
                let mut p_row: BitVec<LocalBits, usize> = BitVec::with_capacity(row[0].0.size);
                for (tile, _) in row {
                    p_row.extend_from_bitslice(&tile.data[i % tile.data.len()]);
                }
                picture.push(p_row);
            }
        }

        // for ti in 0..

        Tile {
            id,
            size: picture.len(),
            data: picture,
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_solve_part_2() {
        let input = include_str!("day20_test");
        let result = solve_part_2(input);
        assert_eq!(result, 273);
    }

    #[test]
    fn test_solve_part_1() {
        let input = include_str!("day20_test");
        let result = solve_part_1(input);
        assert_eq!(result, 20899048083289);
    }

    #[test]
    fn test_field_to_tile() {
        let input = include_str!("day20_test");
        let solved = include_str!("day20_solved");

        let field = solve_puzzle(input);
        let actual = field.to_tile(0);
        let expected = Tile::from_str(solved).unwrap();

        let rotated: Vec<Tile> = Orientation::all()
            .iter()
            .map(|orientation| {
                let mut rotated = actual.clone();
                rotated.rotate(orientation);
                rotated
            })
            .collect();

        assert!(rotated.contains(&expected));
    }

    #[test]
    fn test_count_all_ones() {
        let input = include_str!("day20_solved");

        let tile = Tile::from_str(input).unwrap();
        let expected = input.chars().filter(|c| *c == '#').count();
        assert_eq!(tile.count_all_ones(), expected);
    }

    #[test]
    fn test_tile_get_borders() {
        let input = r"Tile 3079:
        #.#.#####.
        .#..######
        ..#.......
        ######....
        ####.#..#.
        .#...#.##.
        #.#####.##
        ..#.###...
        ..#.......
        ..#.###...";

        let tiles = parse(input);
        let tile = tiles.get(&3079).unwrap();
        assert_eq!(
            tile.get_borders(&Orientation::new(Dir::Up, false)),
            [
                bitvec![1, 0, 1, 0, 1, 1, 1, 1, 1, 0],
                bitvec![0, 1, 0, 0, 0, 0, 1, 0, 0, 0],
                bitvec![0, 0, 0, 1, 1, 1, 0, 1, 0, 0],
                bitvec![0, 0, 0, 1, 0, 1, 1, 0, 0, 1],
            ]
        );
        assert_eq!(
            tile.get_borders(&Orientation::new(Dir::Up, true)),
            [
                bitvec![0, 1, 1, 1, 1, 1, 0, 1, 0, 1],
                bitvec![0, 0, 0, 1, 0, 1, 1, 0, 0, 1],
                bitvec![0, 0, 1, 0, 1, 1, 1, 0, 0, 0],
                bitvec![0, 1, 0, 0, 0, 0, 1, 0, 0, 0],
            ]
        );
    }

    #[test]
    fn test_count_monsters() {
        let input = include_str!("day20_solved");

        let tile = Tile::from_str(input).unwrap();
        assert_eq!(tile.count_all_monster(), 2);
    }

    #[test]
    fn test_tile_trim() {
        let input = r"Tile 3079:
        #.#.#####.
        .#..######
        ..#.......
        ######....
        ####.#..#.
        .#...#.##.
        #.#####.##
        ..#.###...
        ..#.......
        ..#.###...";

        let tiles = parse(input);
        let mut tile = tiles.get(&3079).unwrap().clone();

        assert_eq!(
            tile.get_borders(&Orientation::new(Dir::Up, false))[0],
            bitvec![1, 0, 1, 0, 1, 1, 1, 1, 1, 0]
        );

        tile.trim();
        assert_eq!(tile.size, 8);
        assert_eq!(
            tile.get_borders(&Orientation::new(Dir::Up, false)),
            [
                bitvec![1, 0, 0, 1, 1, 1, 1, 1,],
                bitvec![1, 0, 0, 1, 1, 1, 0, 0,],
                bitvec![0, 0, 0, 0, 0, 0, 1, 0,],
                bitvec![0, 0, 0, 1, 1, 1, 0, 1,],
            ]
        );
        tile.trim();
        assert_eq!(tile.size, 6);
        assert_eq!(
            tile.get_borders(&Orientation::new(Dir::Up, false)),
            [
                bitvec![1, 0, 0, 0, 0, 0,],
                bitvec![0, 0, 0, 1, 0, 0,],
                bitvec![0, 1, 1, 1, 0, 1,],
                bitvec![1, 1, 0, 1, 1, 1,],
            ]
        );
    }
}
