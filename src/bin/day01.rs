use std::{error::Error, fs};

const N: usize = 3;

const fn get_indexes() -> [usize; N] {
    let mut result = [0; 3];
    let mut i: usize = 0;
    // for loops do not work in const fns :c
    loop {
        if i == N {
            break;
        }
        result[i] = i;
        i += 1;
    }
    result
}

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let input = fs::read_to_string("input01")?;
    let numbers: Vec<i32> = input
        .split('\n')
        .filter_map(|s| s.parse::<i32>().ok())
        .collect();

    println!("{}", find(numbers));
    Ok(())
}

fn find(numbers: Vec<i32>) -> i32 {
    let mut indexes = get_indexes();

    loop {
        let elements = indexes.iter().map(|&i| numbers[i]).collect::<Vec<_>>();
        if elements.iter().sum::<i32>() == 2020 {
            return elements.iter().product();
        }

        if !increase(&mut indexes, numbers.len()) {
            panic!("Bad input!")
        }
    }
}

fn increase(indexes: &mut [usize; N], max: usize) -> bool {
    // Find the index's index that can be increased
    let increasable = (0..N).rev().find(|&i| {
        let local_maximum = if i == N - 1 { max } else { indexes[i + 1] };
        local_maximum - indexes[i] > 1
    });
    match increasable {
        Some(i) => {
            let start = indexes[i];
            // Starting from that index set values of all consequential ones
            for (j, index) in indexes.iter_mut().skip(i).enumerate() {
                *index = start + j + 1;
            }
            true
        }
        None => false,
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_find() {
        assert_eq!(find(vec![1721, 979, 366, 299, 675, 1456]), 241861950);
    }

    #[test]
    fn test_increase() {
        let mut indexes = [0, 1, 2];
        assert_eq!(increase(&mut indexes, 4), true);
        assert_eq!(indexes, [0, 1, 3]);
        assert_eq!(increase(&mut indexes, 4), true);
        assert_eq!(indexes, [0, 2, 3]);
        assert_eq!(increase(&mut indexes, 4), true);
        assert_eq!(indexes, [1, 2, 3]);
        assert_eq!(increase(&mut indexes, 4), false);
        assert_eq!(increase(&mut indexes, 4), false);
    }
}
